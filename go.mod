module github.com/pupan-cpe/go-learn

go 1.16

require (
	github.com/gin-gonic/gin v1.7.4 // indirect
	github.com/joho/godotenv v1.4.0 // indirect
)
