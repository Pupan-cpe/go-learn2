package main

import (
	"os"

	"github.com/gin-gonic/gin"
	"github.com/pupan-cpe/go-learn/routes"
)

// var I int
// var float float64
// var k = 10
// var str string
// var isChecked bool

func main() {
	// router := gin.Default()
	// router.Run(":"+os.Getenv("GO_PORT"))
	router := SetupRoutes()
	router.Run(":"+os.Getenv("GO_PORT"))

}

func SetupRoutes()*gin.Engine {
	router:= gin.Default()
	apiv1 :=router.Group("/api/v1")
	routes.InitHomeRoutes(apiv1)
	return router

}



