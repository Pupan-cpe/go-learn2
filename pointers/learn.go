package pointers

import "fmt"

func Test() {
	fmt.Println("test pointers")

	i := 10

	p:= &i

	fmt.Println(*p)

	*p = 21

	fmt.Println(*p)


}